package staffAndEquipment;

public class Trumpetist extends Player {

	public Trumpetist(String name, String mood, String instrument, int skillLevel) {
		super(name, mood, instrument, skillLevel);
	}

	@Override
	public void fixInstrument() {
		// TODO Auto-generated method stub
		System.out.println("Take out valve, clean out with water, polish");
	}

	@Override
	public void practice() {
		// TODO Auto-generated method stub
		System.out.println("Get music, sound proof room, metronome and play");
	}
	
}
