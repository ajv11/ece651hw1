package staffAndEquipment;

public class Celloist extends Player {
	
	public Celloist(String name, String mood, String instrument, int skillLevel){
		super(name,mood,instrument,skillLevel);
	}
	
	@Override
	public void fixInstrument() {
		System.out.println("Take strings off, clean all surfaces, scrub down the "
				+ "fretboard, and tune");
	}

	@Override
	public void practice() {
		System.out.println("Wheel cello into room, take out metronome"
				+ "\ntake out music and bow and play");
		
	}

}
