package staffAndEquipment;

import java.util.List;

public abstract class Instrument {
	public String name;
	private int cost;
	private int age;
	
	public Instrument(String name, int cost, int age){
		this.name=name;
		this.cost=cost;
		this.age=age;
	}
	
	public abstract void cleanUpInstrument();
}
