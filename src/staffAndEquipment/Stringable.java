package staffAndEquipment;

public interface Stringable {
	public abstract void changeStrings();
	public abstract void tuneStrings();
	public abstract void waxStrings();
}
