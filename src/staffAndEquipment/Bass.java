package staffAndEquipment;

public class Bass extends Instrument implements Stringable {

	public Bass(String name, int cost, int age) {
		super(name, cost, age);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void cleanUpInstrument() {
		// TODO Auto-generated method stub
		System.out.println("Cleaned with a rag and wood polish, and a big stick");
	}

	@Override
	public void changeStrings() {
		// TODO Auto-generated method stub
		System.out.println("Strings were changed");
	}

	@Override
	public void tuneStrings() {
		// TODO Auto-generated method stub
		System.out.println("Strings were tunned");
	}

	@Override
	public void waxStrings() {
		// TODO Auto-generated method stub
		System.out.println("Strings were waxed");

	}
}
