package staffAndEquipment;

public abstract class Player {
	public String name;
	private String mood;
	private String instrument;
	private int skillLevel;
	
	public Player(String name, String mood, String instrument, int skillLevel){
		this.name=name;
		this.mood=mood;
		this.instrument=instrument;
		this.skillLevel=skillLevel;
	}
	
	public int getSkillLevel(){
		return skillLevel;
	}
	public String getMood(){
		return mood;
	}
	
	public void updateSkillLevel (int newLevel) throws ArithmeticException{
		if(newLevel<6){
			throw new ArithmeticException("\n\nHey Director! You can't be giving such terrible skill levels "
					+ "these are professionals here! \nWe'll add a couple points to the level for ya.");
			
		}
		if(newLevel<this.skillLevel){
			mood = "Bummed out";
		}
		this.skillLevel=newLevel;
	}
	
	public abstract void fixInstrument();
	public abstract void practice();
}
