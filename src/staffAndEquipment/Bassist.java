package staffAndEquipment;

public class Bassist extends Player{

	public Bassist(String name, String mood, String instrument, int skillLevel) {
		super(name, mood, instrument, skillLevel);
	}
	
	@Override
	public void fixInstrument() {
		System.out.println("Take strings off, clean all surfaces, scrub down the "
				+ "fretboard, and tune");
	}

	@Override
	public void practice() {
		System.out.println("Wheel bass into room, take out metronome"
				+ "\ntake out music and bow and play");
		
	}

}
