package ece651hw1;

import java.util.Random;

import staffAndEquipment.*;


public class Symphony {
	public Object[] instrumentsAndPlayers = new Object[6];
	protected int[] playersSkillLevels = new int[5];
	
	public static void main(String[] args){
		messageToWorld();
		Symphony sym = new Symphony();
		sym.messageToAudience();
		sym.instrumentsAndPlayers[0]=new Bass("Zoa", 10000, 100);
		sym.instrumentsAndPlayers[1]=new Trumpet("Zoom", 10000, 100);
		sym.instrumentsAndPlayers[2]=new Cello("Zorry", 10000, 100);
		sym.instrumentsAndPlayers[3]=new Bassist("Ben","good","Bass",4);
		sym.instrumentsAndPlayers[4]=new Trumpetist("Tom","good","Trumpet",8);
		sym.instrumentsAndPlayers[5]=new Celloist("Cecilia","happy","Cello",9);
		
		System.out.println("We have the following instruments and people:\n");
		for(int i=0;i<3;i++){
			System.out.println( sym.instrumentsAndPlayers[i].getClass()+": "+((Instrument)sym.instrumentsAndPlayers[i]).name);
		}
		for(int i=3;i<6;i++){
			System.out.println( sym.instrumentsAndPlayers[i].getClass()+": "+((Player)sym.instrumentsAndPlayers[i]).name);
		}
		
		System.out.println("\nThe conductor is now going to change the skill levels of the players"
				+ " and some moods changed accordingly:");
		Random rand = new Random();
		for(int i=3;i<6;i++){
			int k = rand.nextInt(10)+1;
			System.out.println("Player: "+((Player)sym.instrumentsAndPlayers[i]).name);
			System.out.print("Old mood: "+((Player)sym.instrumentsAndPlayers[i]).getMood() );			
			try{
				((Player)sym.instrumentsAndPlayers[i]).updateSkillLevel(k);
			}
			catch(Exception e){
				((Player)sym.instrumentsAndPlayers[i]).updateSkillLevel(k+2);
			}
			System.out.print("  New mood: "+((Player)sym.instrumentsAndPlayers[i]).getMood() +"\n\n");
			
		}
	}
	
	private static void messageToWorld(){
		System.out.println("We are organizing a symphony here in this building.");
	}
	private void messageToAudience(){
		System.out.println("We are making a symphony with three players");
	}
}
